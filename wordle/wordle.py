from wordle_solver import wordle_solver


def wordle(**kwargs):
    with open("five-letter-words.txt") as f:
        five_letter_words = f.readlines()

    possible_words = wordle_solver(five_letter_words,
                                   **kwargs)

    with open("possible_words.txt", 'w') as f:
        f.writelines(possible_words)


def nerdle(save_to_file_name="possible-equations", **kwargs):
    with open("eight-digit-equations.txt") as f:
        eight_digit_equations = f.readlines()

    possible_equations = wordle_solver(eight_digit_equations, **kwargs)

    with open(f"{save_to_file_name}.txt", 'w') as f:
        f.writelines(possible_equations)

