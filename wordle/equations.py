def generate_equations():
    """
    All valid equations of length 8 using the operators addition, subtraction, multiplication and division.
    :return: list of strings
    """
    smallest_number = 1
    largest_number = 100
    string_length = 9
    equations_list = []
    for x in range(smallest_number, largest_number):
        for y in range(smallest_number, largest_number):
            addition = x + y
            addition_string = f"{x}+{y}={addition}\n"
            if len(addition_string) == string_length:
                minus_string = f"{addition}-{y}={x}\n"
                equations_list.append(addition_string)
                equations_list.append(minus_string)

            product = x * y
            product_string = f"{x}*{y}={product}\n"
            if len(product_string) == string_length:
                division_string = f"{product}/{y}={x}\n"
                equations_list.append(product_string)
                equations_list.append(division_string)

    operators = ["+", "-", "*", "/"]
    for op1 in operators:
        for op2 in operators:
            for x in range(smallest_number, largest_number):
                for y in range(smallest_number, largest_number):
                    for z in range(smallest_number, largest_number):
                        expression = f"{x}{op1}{y}{op2}{z}"
                        result = eval(expression)
                        equation_string = f"{expression}={int(result)}\n"
                        if result >= 0 and result == int(result) and len(equation_string) == string_length:
                            equations_list.append(equation_string)

    return equations_list


if __name__ == "__main__":
    # numbers = [2, 3, 4, 6, 9]
    # digits = [0] + numbers
    #
    # equations = [f"{n}{d}*{m}={(10*n + d) * m}\n" for n in numbers for d in digits for m in numbers]

    equations = generate_equations()

    with open(f"8_digit_equations.txt", 'w') as file:
        file.writelines(equations)
