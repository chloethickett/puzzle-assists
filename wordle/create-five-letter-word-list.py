with open("five-letter-words.txt") as f:
    word_list = f.readlines()

word5 = []
for word in word_list:
    if len(word) == 6:
        word5.append(word)

word5.sort()

with open("five-letter-words.txt", 'w') as f:
    f.writelines(word5)
