def wordle_solver(words,
                  known_letters=None,
                  impossible_letters='',
                  exact_letter_frequency=None,
                  at_least_letter_frequency=None):
    """

    :param words: list of strings ending with a newline character
    :param known_letters: list of length len(words)-1 of tuples of length 2 of strings,
        the first string corresponding to green letter in that place,
        the second corresponding to amber letters in that place
    :param impossible_letters: string of letters that have been blacked out
    :param exact_letter_frequency: dictionary with string keys and integer values corresponding to the exact number
        of occurrences of the letters in the string in the desired word.
    :param at_least_letter_frequency: dictionary with string keys and integer values such that there must be at
        least that number of occurrences of the letters in the string in the desired word.
    :return: list of strings ending with a newline character
    """
    if known_letters is None:
        known_letters = [('', '')]
    if exact_letter_frequency is None:
        exact_letter_frequency = {'': 0}
    if at_least_letter_frequency is None:
        at_least_letter_frequency = {'': 0}

    somewhere_letters = ''.join([letters[1] for letters in known_letters])
    somewhere_letters = ''.join(set(somewhere_letters))

    exact_letter_frequency[impossible_letters] = 0
    at_least_letter_frequency[somewhere_letters] = 1

    possible_words = []
    for word in words:
        if word_check(word,
                      known_places=known_letters,
                      exact_letter_frequency=exact_letter_frequency,
                      at_least_letter_frequency=at_least_letter_frequency):
            possible_words.append(word)

    return possible_words


def word_check(word, known_places=None,
               exact_letter_frequency=None,
               at_least_letter_frequency=None):
    """

    :param word: string ending with a newline character
    :param known_places: list of length len(words)-1 of tuples of length 2 of strings,
        the first string corresponding letter known to be in that place,
        the second corresponding to letters known not to be in that place.
    :param exact_letter_frequency: dictionary with string keys and integer values corresponding to the exact number
        of occurrences of the letters in the string in the desired word.
    :param at_least_letter_frequency: dictionary with string keys and integer values such that there must be at
        least that number of occurrences of the letters in the string in the desired word.
    :return: Boolean, True if word satisfies the conditions of the word, else False.
    """
    if known_places is None:
        known_places = [('', '')]
    if exact_letter_frequency is None:
        exact_letter_frequency = {'': 0}
    if at_least_letter_frequency is None:
        at_least_letter_frequency = {'': 0}

    for i, letters in enumerate(known_places):
        if letters[0]:
            if word[i] != letters[0]:
                return False
        else:
            if word[i] in letters[1]:
                return False

    for letters, frequency in exact_letter_frequency.items():
        for letter in letters:
            if word.count(letter) != frequency:
                return False

    for letters, frequency in at_least_letter_frequency.items():
        for letter in letters:
            if word.count(letter) < frequency:
                return False

    return True
