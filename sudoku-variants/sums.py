def combinations(number_of_numbers, largest_number, smallest_number):
    """
    All nCk (n choose k) combinations of integers, where (n = largest number - smallest number + 1)
     and (k = number_of_numbers).

    :param number_of_numbers: integer representing the length of the combinations (k)
    :param largest_number: integer
    :param smallest_number: integer
    :return: list of list of integers
    """
    m = largest_number - number_of_numbers + 2
    sums_list = [[i] for i in range(smallest_number, m)]
    for i in range(1, number_of_numbers):
        sums_list = combinations_pass(sums_list, m + i)
    return sums_list


def combinations_pass(sums_list, largest_number):
    return [numbers + [i] for numbers in sums_list for i in range(numbers[-1] + 1, largest_number)]


def sum_list(number_of_numbers, largest_number=9, smallest_number=1, check_condition=None,
             line_start=None, line_end=None, separator='+'):
    """
    All sums of a certain length (number_of_numbers) of all integers between smallest_number and largest_number
    sorted by sum total.

    :param number_of_numbers: integer or list of integers representing the length of sums wanted
    :param largest_number: integer
    :param smallest_number: integer
    :param check_condition: function taking one argument of a list of integers representing the numbers to be summed.
    The function returns true if that combination will be included in the list, and false if not.
    :param line_start: function taking one argument and returns a string that will be used at the beginning of each line
    :param line_end: function taking one argument and returns a string that will be used at the end of each line
    :param separator: string separating the each number within each sum
    :return: list of tuples as (string, integer)
    """
    def sum_index(e):
        return e[1]
    if check_condition is None:
        def check_condition(e): return True
    if line_end is None:
        def line_end(e): return '\n'
    if line_start is None:
        def line_start(e): return ''
    if isinstance(number_of_numbers, int):
        number_of_numbers = [number_of_numbers]

    sums_list = []
    for number in number_of_numbers:
        combinations_list = combinations(number, largest_number, smallest_number)
        sums_list += [(line_start(numbers) + separator.join(map(str, numbers)) + line_end(numbers), sum(numbers))
                      if check_condition(numbers) else ('', 0)
                      for numbers in combinations_list]
    sums_list.sort(key=sum_index)
    return sums_list


def sum_combinations(number_of_numbers, largest_number=9, smallest_number=1, check_condition=None,
                     extra_space=True, subtitles=True, equal_sign=False):
    """

    :param number_of_numbers: integer or list of integers representing the length of sums wanted
    :param largest_number: integer
    :param smallest_number: integer
    :param check_condition: function taking one argument of a list of integers representing the numbers to be summed.
    The function returns true if that combination will be included in the list, and false if not.
    :param extra_space: Boolean if True adds a line between each sum total
    :param subtitles: Boolean if True adds a subtitle of the sum between different sum values
    :param equal_sign: Boolean if True adds an equals sign and sum total to each sum
    :return: list of strings
    """
    if equal_sign:
        def line_end(number_list):
            return f'={sum(number_list)}\n'
    else:
        def line_end(number_list):
            return '\n'

    sums_list = sum_list(number_of_numbers, largest_number, smallest_number,
                         check_condition=check_condition, line_end=line_end)
    sum_strings, sum_totals = map(list, zip(*sums_list))

    if extra_space or subtitles:
        old_total = sum_totals[0]
        if subtitles:
            sum_strings[0] = f'{old_total}\n' + sum_strings[0]
        for i, total in enumerate(sum_totals):
            if total != old_total:
                if extra_space:
                    sum_strings[i-1] += '\n'
                if subtitles:
                    sum_strings[i-1] += f'{total}\n'
                old_total = total
    return sum_strings


if __name__ == "__main__":
    # number = 5
    # sums = sum_combinations(number)
    # with open(f"sums{number}.txt", 'w') as file:
    #     file.writelines(sums)

    def length_included(numbers):
        return len(numbers) in numbers
    sums = sum_combinations(list(range(2, 10)), smallest_number=1, largest_number=9,
                            subtitles=True, equal_sign=False, check_condition=length_included)
    with open(f"sums/all_sums_with_length.txt", 'w') as file:
        file.writelines(sums)
